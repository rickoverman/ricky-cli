'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const art = require('./lib/art.js');

module.exports = class extends Generator {
  prompting() {
    this.log(`Ricky CLI ${chalk.green('1.0.0')}\n`);
    this.log(`${chalk.yellow('Available commands:')}`);

    const cs = commands();
    for(const c of cs){
      this.log(`${chalk.green('  '+c.title)}\t\t\t${c.description}`);
    }

  }


};

function commands(){
  return [
    {title: 'list', description: 'Lists commands'}

  ];
}