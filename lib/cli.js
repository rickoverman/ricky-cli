#!/usr/bin/env node

const path = require('path')
var yeoman = require('yeoman-environment');
var env = yeoman.createEnv();

env.register(require.resolve(path.join(__dirname, '../generators/default/list')), 'list');
env.register(require.resolve(path.join(__dirname, '../generators/default/app')), 'default:app');
env.register(require.resolve(path.join(__dirname, '../generators/pb/app')), 'pb:app');

// Delete the 0 and 1 argument (node and script.js)
var args = process.argv.splice(process.execArgv.length + 2);

// Retrieve the first argument
var namespace = args[0] ? args[0] : 'default:app';

env.run(namespace);